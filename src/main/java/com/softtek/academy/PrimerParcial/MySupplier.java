package com.softtek.academy.PrimerParcial;

@FunctionalInterface
public interface MySupplier<C extends Device> {

	public C get(long deviceId,String name,String description,long manufacturerId,long colorId,
			String comments);
}