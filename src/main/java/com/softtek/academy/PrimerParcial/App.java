package com.softtek.academy.PrimerParcial;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

    	Connection con=Conexion.getConnection();//singleton pattern
    	List<Device> result = new ArrayList<>();
    	if(con!=null) {
    		System.out.println("Conexion exitosa a la base de datos");
    	String SQL_SELECT="SELECT * FROM DEVICE";
    	
    	try {
        	
            PreparedStatement preparedStatement = con.prepareStatement(SQL_SELECT);
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while (resultSet.next()) { 
            	long deviceId=resultSet.getLong("deviceId");
            	String name=resultSet.getString("name");
            	String description=resultSet.getString("description");
            	long manufacturerId=resultSet.getLong("manufacturerId");
            	long colorId=resultSet.getLong("colorId");
            	String comments=resultSet.getString("comments");

            	
                MySupplier<Device> supplier = (d,n,e,m,x,y)
                		-> new Device(d,n,e,m,x,y);
                
                Device device = supplier.get(deviceId,name,description,manufacturerId,colorId,
            			 comments);
                result.add(device);    
            }
            
/////////////////////////////////////////////////////////////////////////////
            
            String value = "Laptop";
            List<String> name = result.stream()
                	.map(Device::getName)
                	.filter(line -> value.equals(line))
                	.collect(Collectors.toList());
            
                name.forEach(System.out::println);

            
/////////////////////////////////////////////////////////////////////////////
                
                
            BiFunction<List<Device>, Long, Device> searchFunction = (list, l) -> {
            	for(Device c : list) {
            		if(l == c.getDeviceId()) {
            			return c;
            		}
            	}
            	return null;
            };
            Device c = searchFunction.apply(result, 3L);
            System.out.println(c);
            
            
/////////////////////////////////////////////////////////////////////////////
            
            Map<Object, Object> resAsMap = result.stream()
                	.collect(
                			Collectors.toMap(
                				
                					(c1) -> ((Device)c1).getDeviceId(),
                					Device::getName)
                			);
                		
                resAsMap.forEach((k, v) -> System.out.println("Key: " + k + " , value:" + v));
                
/////////////////////////////////////////////////////////////////////////////
		} catch (Exception e) {
			System.out.println(e);
		}
        }
    	
    	else
    		System.out.println("ERROR de conexion");
    }
}
