package com.softtek.academy.PrimerParcial;


public class Device {

	private long deviceId;
	private String name;
	private String description;
	private long manufacturerId;
	private long colorId;
	private String comments;
	
	public Device(long deviceId,String name,String description,long manufacturerId,long colorId,
			String comments) {
		super();
		this.deviceId=deviceId;
		this.name=name;
		this.description=description;
		this.manufacturerId=manufacturerId;
		this.colorId=colorId;
		this.comments=comments;
	}

	public long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(long deviceId) {
		this.deviceId = deviceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getManufacturerId() {
		return manufacturerId;
	}

	public void setManufacturerId(long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}

	public long getColorId() {
		return colorId;
	}

	public void setColorId(long colorId) {
		this.colorId = colorId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	@Override
	public String toString() {
		return "Device= "+" "+deviceId+", "+name+", "+description+
				", "+manufacturerId+", "+colorId+", "+comments;
	}
	
}