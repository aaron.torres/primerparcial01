package com.softtek.academy.PrimerParcial;


import java.sql.*;

public class Conexion
{
	private static Connection conn;
	
	private Conexion(){}
	
	public static Connection getConnection()
	{
		try 
		{
			if(conn==null)
			{
				//Class.forName("com.mysql.cj.jdbc.Driver");
				conn = DriverManager.getConnection
						("jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", 
								"root", "1234");
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return conn;
	}
}